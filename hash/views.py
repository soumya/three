from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .handlers import ReverseHasher

def reverse(request):
    output = "The original string for hash %s is %s"
    errmsg = "Please enter valid input"
    hash_val = request.GET.get('hash','')
    try:
        hash_val = int(hash_val)
    
        if hash_val:
            original = ReverseHasher().unhash(hash_val)
            output = output %( hash_val, original) 
            return HttpResponse(output)
        else:
            raise Exception('Empty input')

    except Exception,e:
        return HttpResponse(errmsg,status_code=500)
    
