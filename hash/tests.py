from django.test import TestCase
from .handler import (
    HashGenerator, ReverseHasher
)
# Create your tests here.

class HashGeneratorTests(TestCase):
    
    def test_hash_generator_is_correct(self):
        hasher = HashGenerator()
        sample_string = "leepadg"
        hash_val = hasher.hash(sample_string)
        self.assertIs(hash_val==680131659347 , True)

    def test_reverse_hash_is_correct(self):
        reverse_hasher= ReverseHasher()
        hash_val = 680131659347
        original = reverse_hasher.unhash(hash_val)
        self.assertIs(original=="leepadg",True)
        self.assertIs("lawnmower" == reverse_hasher.unhash(930846109532517), True)
